import styled from "styled-components";

const ProjectFormWrapper = styled.div`
  @media (min-width: 360px) {
    display: flex;
    flex-direction: column;
    & .titleDiv {
      margin-bottom: 20px;
      padding-bottom: 20px;
      border-bottom: 0.6000000238418579px solid #bcc3ca;
    }
    & .firstP {
      font-weight: 600;
      border-bottom: 3px solid #c10e21;
      width: fit-content;
      padding-bottom: 5px;
    }
    & input {
      /* width: 635px; */
      border: 0.6000000238418579px solid #bcc3ca;
      color: #bcc3ca;
    }
    & textarea {
      /* width: 635px; */
      border: 0.6000000238418579px solid #bcc3ca;
    }
    /* & form {
      border-bottom: 1px solid #bcc3ca;
      padding-bottom: 30px;
    } */

    & .bottomHr {
      margin-top: 40px;
    }

    & .formButtonDiv {
      display: flex;
      justify-content: flex-end;
      margin-top: 40px;

      & button {
        width: 128px;
        height: 40px;
        border-radius: 20px;
        margin-right: 20px;
        border: 1px solid #c10e21;
      }

      & .cancelBtn {
        background: white;
        color: #c10e21;
      }

      & .saveBtn {
        background: #c10e21;
        color: white;
        border: none;
      }
    }
  }
  & .titleDiv {
    margin-bottom: 20px;
    padding-bottom: 20px;
    border-bottom: 0.6000000238418579px solid #bcc3ca;
  }
  & .firstP {
    font-weight: 600;
    border-bottom: 3px solid #c10e21;
    width: fit-content;
    padding-bottom: 5px;
    margin-top: 15px;
  }
  & input {
    /*  max-width: 635px; */
    /* width: 635px; */
    border: 0.6000000238418579px solid #bcc3ca;
    color: #bcc3ca;
  }
  & textarea {
    /* max-width: 635px; */
    /* width:635px; */
    border: 0.6000000238418579px solid #bcc3ca;
  }
  & .formButtonDiv {
    display: flex;
    margin: 10px;

    & button {
      width: 128px;
      height: 40px;
      border-radius: 20px;
      margin-right: 20px;
      border: 1px solid #c10e21;
      @media (max-width: 320px) {
        margin-left: auto;
        margin-right: auto;
        width: 80%;
      }
    }

    & .cancelBtn {
      background: white;
      color: #c10e21;
    }

    & .saveBtn {
      background: #c10e21;
      color: white;
      border: none;
      @media (max-width: 320px) {
        margin-top: 10px;
      }
    }
    @media (max-width: 320px) {
      display: flex;
      flex-direction: column;
    }
  }
`;

export default ProjectFormWrapper;
