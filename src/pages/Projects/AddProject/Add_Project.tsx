/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-debugger, no-console */
import React from "react";
import Loader from "components/Loader";
import ScrollToTop from "components/scrollToTop";
import Toast from "components/Toast";
import AsideBar from "components/AsideBar";
import { Header } from "components/Header";
import Table from "components/Table/Table";
import Pagination from "components/Pagination";
import { Link } from "react-router-dom";
import { DashboardWrapper } from "../../Dashboard/styled";
import { Footer } from "../../../components/Footer/index";
import ProjectFormWrapper from "./styled";

const AddProject = () => {
  /* const signin = () => {
    window.userManager
      .signinRedirect()
      .then((data: any) => {
        console.log("signinRedirect ok..................", data);
      })
      .catch((err: any) => {
        console.log("signinRedirect error:", err);
      });
  }; 
  */
  return (
    <DashboardWrapper className="container-fluid d-flex flex-column w-100 p-0">
      <div className="d-flex" id="maincontent">
        <div className="d-flex content-container">
          <AsideBar />
          <div className="left-container flex-grow-1">
            <Header />
            <ScrollToTop />
            <main className="">
              <ProjectFormWrapper>
                <div className="titleDiv">
                  <h3>Add Project</h3>
                </div>
                <div className="formDiv">
                  <form>
                    <p className="firstP">Project detail</p>
                    <div className="form-group row">
                      <label className="col-sm-3 col-form-label">Name</label>
                      <div className="col-sm-8">
                        <input
                          type="text"
                          className="form-control"
                          id="projectName"
                          aria-label="Enter your Name"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-3 col-form-label">
                        Project Description
                      </label>
                      <div className="col-sm-8">
                        <textarea
                          className="form-control"
                          id="projectDesc"
                          aria-label="Enter Project description"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-3 col-form-label">Scope</label>
                      <div className="col-sm-8">
                        <input
                          type="text"
                          className="form-control"
                          id="scope"
                          aria-label="Enter scope"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-3 col-form-label">Budget</label>
                      <div className="col-sm-8">
                        <input
                          type="number"
                          className="form-control"
                          id="budget"
                          aria-label="Enter budget"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-3 col-form-label">
                        Project Manager
                      </label>
                      <div className="col-sm-8">
                        <input
                          type="text"
                          className="form-control"
                          id="projectManager"
                          aria-label="Enter project manager"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-3 col-form-label">
                        Team Size
                      </label>
                      <div className="col-sm-8">
                        <input
                          type="number"
                          className="form-control"
                          id="teamSize"
                          aria-label="Enter the team size"
                        />
                      </div>
                    </div>
                    <p className="firstP">Schedule</p>
                    <div className="form-group row">
                      <label className="col-sm-3 col-form-label">
                        Start Date
                      </label>
                      <div className="col-sm-8">
                        <input
                          type="date"
                          className="form-control"
                          id="startDate"
                          aria-label="Select Start Date"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-3 col-form-label">
                        End Date
                      </label>
                      <div className="col-sm-8">
                        <input
                          type="date"
                          className="form-control"
                          id="endDate"
                          aria-label="Select Start Date"
                        />
                      </div>
                    </div>
                    <p className="firstP">Login Details</p>
                    <div className="form-group row">
                      <label className="col-sm-3 col-form-label">
                        User Name
                      </label>
                      <div className="col-sm-8">
                        <input
                          type="text"
                          className="form-control"
                          id="userName"
                          aria-label="Enter user email"
                        />
                      </div>
                    </div>
                    <div id="lastFormDiv" className="form-group row">
                      <label className="col-sm-3 col-form-label">
                        Password
                      </label>
                      <div className="col-sm-8">
                        <input
                          type="password"
                          className="form-control"
                          id="password"
                          aria-label="Enter password"
                        />
                      </div>
                    </div>
                    <hr className="bottomHr" />
                    <div className="formButtonDiv">
                      <button className="cancelBtn" type="button">
                        Cancel
                      </button>
                      <button className="saveBtn" type="submit">
                        Save
                      </button>
                    </div>
                  </form>
                </div>
              </ProjectFormWrapper>
            </main>
            {/* <Pagination pageNumber=1 pageSize=5 totalCount=6 isFirst=true isLast=false /> */}
          </div>
        </div>
      </div>
      <Footer />
      <Toast />
      <Loader />
    </DashboardWrapper>
  );
};

export default AddProject;
