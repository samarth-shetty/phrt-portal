/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-debugger, no-console */
import React from "react";
import Loader from "components/Loader";
import ScrollToTop from "components/scrollToTop";
import Toast from "components/Toast";
import AsideBar from "components/AsideBar";
import { Header } from "components/Header";
import Table from "components/Table/Table";
import Pagination from "components/Pagination";
import { Link } from "react-router-dom";
import { DashboardWrapper } from "../../Dashboard/styled";
import DashboardWrapper1 from "./styled";
import { Footer } from "../../../components/Footer/index";

const ProjectName = () => {
  /* const signin = () => {
    window.userManager
      .signinRedirect()
      .then((data: any) => {
        console.log("signinRedirect ok..................", data);
      })
      .catch((err: any) => {
        console.log("signinRedirect error:", err);
      });
  }; 
  */
  return (
    <DashboardWrapper className="container-fluid d-flex flex-column w-100 p-0">
      <div className="d-flex" id="maincontent">
        <div className="d-flex content-container">
          <AsideBar />
          <div className="left-container flex-grow-1">
            <Header />
            <ScrollToTop />
            <main className="">
              <DashboardWrapper1>
                <nav className="headNav">
                  {/* Project Name goes here */}
                  <p>ShopCPR</p>
                  <button type="submit">Add Report</button>
                </nav>
                <div className="progressDiv">
                  <div className="graphDiv">
                    {/* Graph will go here will go here */}
                    <img
                      src={`${process.env.PUBLIC_URL}/images/dummy_graph.png`}
                      alt=""
                    />
                  </div>
                  <div className="details">
                    {/* Top details regarfing start date etc goes here */}
                    <div className="detail1">
                      <div>
                        <span className="spanEle">
                          <span className="spanIn">Start Date</span>
                          <span className="colonOp" aria-hidden="true">
                            :
                          </span>
                          <span className="spanIn">21 March 2021</span>
                        </span>
                        <span className="spanEle">
                          <span className="spanIn">Status </span>
                          <span className="colonOp" aria-hidden="true">
                            :
                          </span>
                          <span className="spanIn">In Progress</span>
                        </span>
                      </div>
                      <div>
                        <span className="spanEle">
                          <span className="spanIn">Team Size</span>
                          <span className="colonOp" aria-hidden="true">
                            :
                          </span>
                          <span className="spanIn">62</span>
                        </span>
                        <span className="spanEle">
                          <span className="spanIn">Project Manager</span>
                          <span className="colonOp" aria-hidden="true">
                            :
                          </span>
                          <span className="spanIn">Girish Kumar</span>
                        </span>
                      </div>
                    </div>
                    <div className="detail2">
                      <p className="detailP">Project Details</p>
                      <p>
                        {" "}
                        {/* project details will go here */}
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Aperiam eaque hic autem numquam porro, consequatur cum
                        architecto. Voluptates ex reprehenderit architecto,
                        consectetur natus nulla eos, voluptas ea hic nisi
                        quisquam. lor
                      </p>
                    </div>
                  </div>
                </div>
                <nav className="filterBar">
                  <div className="filterByDiv">
                    <div>
                      <p className="filterByPtag">Filter By</p>
                    </div>
                    {/* selection of dates here for filtering */}
                    <select
                      className="select1"
                      aria-label="Select the year for filtering the projects"
                    >
                      <option value="jan">2020</option>
                      <option value="jan">2021</option>
                    </select>
                    <select
                      className="select2"
                      aria-label="Select the month for filtering the projects"
                    >
                      <option value="jan">January</option>
                      <option value="jan">February</option>
                    </select>
                  </div>
                  <div className="statusName">
                    <p> No Status Change </p>
                    <p>Not Updated </p>
                  </div>
                </nav>
                <div>
                  <table className="aui-responsive-status-table">
                    <tbody>
                      <tr className="aui-table-status-red">
                        <td data-title="Week 1">
                          <div className="aui-td">
                            <span
                              className="hidableSpan"
                              style={{
                                color: "#C10E21",
                                fontWeight: 600,
                                textDecoration: "underline",
                              }}
                            >
                              Week 1
                            </span>
                            <span>November 1st to 6th</span>
                          </div>
                        </td>
                        <td data-title="Scope">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Scope{" "}
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/right-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="noChangeSpan">
                              No Change{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Quality">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Quality
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/top-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="changeSpan">
                              <span>
                                Changes
                                <span className="percentageSpan">20%</span>{" "}
                              </span>{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Schedule">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Schedule
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/right-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="noChangeSpan">
                              No Change{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Budget">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Budget
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/top-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="changeSpan">
                              <span>
                                Changes
                                <span className="percentageSpan">20%</span>{" "}
                              </span>{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Overall Health">
                          <div className="overallH aui-td">
                            <div className="overallHinner">
                              <span
                                className="hidableSpan"
                                style={{ color: "#C10E21", fontWeight: 600 }}
                              >
                                Overall Health
                              </span>
                              <span className="noChangeSpan">
                                01/11/2021 to 06/11/2021
                              </span>
                            </div>
                            <div className="healthgraph">
                              <img
                                src={`${process.env.PUBLIC_URL}/images/progress-graph.png`}
                                alt=""
                              />
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr className="aui-table-status-grey">
                        <td data-title="Week 1">
                          <div className="aui-td">
                            <span
                              className="hidableSpan"
                              style={{
                                color: "#C10E21",
                                fontWeight: 600,
                                textDecoration: "underline",
                              }}
                            >
                              Week 1
                            </span>
                            <span>November 1st to 6th</span>
                          </div>
                        </td>
                        <td data-title="Scope">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Scope
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/right-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="noChangeSpan">
                              No Change
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Quality">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Quality
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/top-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="changeSpan">
                              <span>
                                Changes
                                <span className="percentageSpan">20%</span>{" "}
                              </span>{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Schedule">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Schedule
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/right-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="noChangeSpan">
                              No Change{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Budget">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Budget
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/top-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="changeSpan">
                              <span>
                                Changes
                                <span className="percentageSpan">20%</span>{" "}
                              </span>{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Overall Health">
                          <div className="overallH aui-td">
                            <div className="overallHinner">
                              <span
                                className="hidableSpan"
                                style={{ color: "#C10E21", fontWeight: 600 }}
                              >
                                Overall Health
                              </span>
                              <span className="noChangeSpan">
                                01/11/2021 to 06/11/2021
                              </span>
                            </div>
                            <div className="healthgraph">
                              <img
                                src={`${process.env.PUBLIC_URL}/images/progress-graph.png`}
                                alt=""
                              />
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr className="aui-table-status-red">
                        <td data-title="Week 1">
                          <div className="aui-td">
                            <span
                              className="hidableSpan"
                              style={{
                                color: "#C10E21",
                                fontWeight: 600,
                                textDecoration: "underline",
                              }}
                            >
                              Week 1
                            </span>
                            <span>November 1st to 6th</span>
                          </div>
                        </td>
                        <td data-title="Scope">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Scope{" "}
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/right-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="noChangeSpan">
                              No Change{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Quality">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Quality
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/top-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="changeSpan">
                              <span>
                                Changes
                                <span className="percentageSpan">20%</span>{" "}
                              </span>{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Schedule">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Schedule
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/right-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="noChangeSpan">
                              No Change{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Budget">
                          <div className="aui-td">
                            <span className="hidableSpan">
                              Budget
                              <span className="arrowSpan">
                                <img
                                  src={`${process.env.PUBLIC_URL}/images/top-arrow.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                            <span className="changeSpan">
                              <span>
                                Changes
                                <span className="percentageSpan">20%</span>{" "}
                              </span>{" "}
                              <span className="cmtSpan">
                                <img
                                  className="commentImage"
                                  src={`${process.env.PUBLIC_URL}/images/comment.svg`}
                                  alt=""
                                />
                              </span>
                            </span>
                          </div>
                        </td>
                        <td data-title="Overall Health">
                          <div className="overallH aui-td">
                            <div className="overallHinner">
                              <span
                                className="hidableSpan"
                                style={{ color: "#C10E21", fontWeight: 600 }}
                              >
                                Overall Health
                              </span>
                              <span className="noChangeSpan">
                                01/11/2021 to 06/11/2021
                              </span>
                            </div>
                            <div className="healthgraph">
                              <img
                                src={`${process.env.PUBLIC_URL}/images/progress-graph.png`}
                                alt=""
                              />
                            </div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </DashboardWrapper1>
            </main>
            {/* <Pagination pageNumber=1 pageSize=5 totalCount=6 isFirst=true isLast=false /> */}
          </div>
        </div>
      </div>
      <Footer />
      <Toast />
      <Loader />
    </DashboardWrapper>
  );
};

export default ProjectName;
