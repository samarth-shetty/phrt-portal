import styled from "styled-components";

const DashboardWrapper1 = styled.div`
  @media (min-width: 768px) {
    display: flex;
    flex-direction: column;
    & .headNav {
      display: flex;
      justify-content: space-between;
      margin-bottom: 20px;
      & button {
        background: #c10e21 0% 0% no-repeat padding-box;
        border: #c10e21;
        border-radius: 21px;
        opacity: 1;
        width: 159px !important;
        color: white;
        height: 40px;
      }
      & p {
        font-size: 26px;
        color: #222328;
      }
    }

    & .progressDiv {
      display: flex;
      & .graphDiv {
        margin-right: 35px;
      }
      & .details {
        display: flex;
        flex-direction: column;

        & .detail1 {
          margin-bottom: 20px;
          margin-top: 20px;
          display: flex;
          justify-content: space-between;

          & div {
            display: flex;
            width: 47%;
            flex-direction: column;

            & .spanEle {
              display: flex;
              justify-content: space-between;
              height: max-content;
              margin-bottom: 10px;
              width: 100%;

              & .spanIn {
                display: flex;
                width: 34%;
                justify-content: space-between;
              }
            }
          }
        }
        & .detail2 {
          margin-bottom: 20px;
          margin-top: -20px;
          & .detailP {
            font-weight: 600;
          }
        }
      }
    }

    & .filterBar {
      display: flex;
      justify-content: space-between;
      align-items: baseline;

      & .filterByDiv {
        display: flex;
      }

      & .filterByPtag {
        font-weight: 600;
        padding: 14px;
        margin-right: 5px;
      }

      & select {
        width: 194px;
        height: 40px;
        margin-top: 8px;
        padding-left: 10px;
        padding-right: 10px;
        border-color: #bcc3ca;
      }
      & .select2 {
        margin-left: 20px;
      }

      & .statusName {
        display: flex;
        justify-content: space-between;

        & p {
          margin-left: 30px;
        }
      }
    }
  }

  & .headNav {
    display: flex;
    margin-bottom: 20px;
    justify-content: space-between;

    & button {
      background: #c10e21 0% 0% no-repeat padding-box;
      border: #c10e21;
      border-radius: 21px;
      opacity: 1;
      width: 140px;
      color: white;
      height: 40px;

      @media (max-width: 320px) {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 100%;
      }
    }

    & p {
      font-size: 26px;
      color: #222328;

      @media (max-width: 320px) {
        margin-right: auto;
        margin-left: auto;
      }
    }
    @media (max-width: 320px) {
      display: flex;
      flex-direction: column;
    }
  }

  & .progressDiv .graphDiv img {
    display: block;
    margin-left: auto;
    margin-right: auto;
  }

  & .detail1 {
    margin-left: -4px;
  }

  & .detail1 div {
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    & .spanEle {
      display: flex;
      justify-content: space-between;
      align-self: start;
      height: 35px;
      width: 100%;

      & .spanIn {
        display: flex;
        width: 34%;
        justify-content: space-between;
        margin: 5px;
      }
    }
  }

  & .detailP {
    font-weight: 600;
    margin-top: 20px;
  }

  & .filterByPtag {
    font-weight: 600;
  }

  & select {
    width: 100%;
    height: 40px;
    margin-bottom: 10px;
    border-color: #bcc3ca;
  }

  & .aui-td {
    display: flex;
    flex-direction: column;

    & span {
      width: 100%;
      margin-bottom: 5px;
      & .arrowSpan {
        margin-left: 20px;
      }
      & .cmtSpan {
        flex-direction: row-reverse !important;
        & .commentImage {
          width: 16.18px;
          height: 14.18px;
          float: right;
          cursor: pointer;
        }
      }
    }

    & .changeSpan {
      color: #eb0f25;
      & .percentageSpan {
        font-weight: 600;
        margin-left: 5px;
      }
      @media (max-width: 1300px) and (min-width: 992px) {
        & span {
          display: flex;
          flex-direction: column;
        }
        height: 100%;
        justify-content: space-between;
        display: flex;
        flex-direction: column;
        & .percentageSpan {
          font-weight: 600;
          margin-left: 5px;
        }
      }
    }

    & .noChangeSpan {
      @media (max-width: 1300px) and (min-width: 992px) {
        display: flex;
        flex-direction: column;
        height: 100%;
        justify-content: space-between;
      }
    }

    @media (max-width: 992px) {
      & .hidableSpan {
        display: none;
      }
    }
  }

  & .overallH {
    display: flex;
    flex-direction: row;
    justify-content: space-between;

    @media (max-width: 992px) and (min-width: 576px) {
      display: flex !important;
    }
    @media (max-width: 1200px) and (min-width: 992px) {
      display: flex;
      flex-direction: column;
    }

    & .overallHinner {
      display: flex;
      flex-direction: column;
      & span {
        width: 100%;
      }
    }

    & img {
      width: 62px;
      height: 62px;
    }
  }
`;

export default DashboardWrapper1;
