import styled from "styled-components";

export const DashboardWrapper = styled.div`
  min-height: 100vh;
  #maincontent {
    margin-inline: 20px;
    @media (min-width: 992px) {
      margin: 18px 21px 18px 21px;
      height: 100%;
      flex-grow: 1;
    }
  }
  .flex-prop-media {
    flex-direction: column;
    @media (min-width: 992px) {
      flex-direction: row;
    }
  }
  .content-container {
    display: flex;
    width: 100%;
    flex-direction: column;
    @media (min-width: 992px) {
      flex-direction: row;
    }
  }
  .left-container {
    padding: 0px;
    @media (min-width: 991px) {
      padding: 19px 10px 19px 31px;
    }
  }
  .heading-bar {
    padding-bottom: 31px;
  }
  .font-styled {
    font: normal normal 600 22px/49px Montserrat;
    letter-spacing: 0px;
    color: #000000;
    opacity: 1;
  }
  button.btn-height {
    height: auto;
    max-height: 40px;
    width: 100%;
    max-width: 160px;
    padding: 8px 30px 8px 30px;
  }
  .Red {
    color: #eb0f25;
    font-weight: normal;
  }
  .Green {
    color: #0d8200;
    font-weight: normal;
  }
`;

export const AnotherWraper = styled.div``;
