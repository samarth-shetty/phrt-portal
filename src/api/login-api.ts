/* eslint-disable no-console */
import axios from "axios";
import React, { useState } from "react";

const Login = (email: string, password: string) => {
  return axios
    .post("http://localhost:8080/account/v1/adminLogin", {
      email,
      password,
    })
    .then((response) => {
      if (response.data.access_token) {
        localStorage.setItem(
          "user",
          JSON.stringify(response.data.access_token)
        );
      }
      return response.data;
    })
    .catch((error) => {
      /* console.log(error);
      console.log(error.response.status);
      console.log(error.response.data.error);
      console.log(error.response.data.error.details[0].message);
      console.log(error.message); */
      throw error;
    });
};

export default Login;
