/* eslint-disable prettier/prettier */
import React from "react";
import { MyAccount } from "./styled";

function MyAccounts() {
    return (
        <MyAccount className="navbar aui-profile d-inline-block " id="MyAccount">
            <button
                aria-label="user-profile"
                className="btn btn-text aui-dropdown aha-icon-arrow-down dropdown-toggle"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                type="button"
            >
                <i className="pr-4" />
                My Account
            </button>
            <ul className="aui-topright-arrow dropdown-menu p-lg-0 header-dropdown-menu">
                <li className="account-list">
                    <a className="dropdown-item py-2" href="/">
                        Profile Settings
                    </a>
                </li>
                <li className="account-list py-2">
                    <a className="dropdown-item" href="/">
                        Sign Out
                    </a>
                </li>
            </ul>
        </MyAccount>
    );
}
// p-0 mr - 3 mt - 2
export default MyAccounts;
